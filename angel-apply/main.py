import time
import logging

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

from utils import apply_and_exit
from settings import search_urls, frequency

index = len(search_urls) - 1
while True:
    logging.info("entered main")
    apply_and_exit(search_urls[index])
    index = len(search_urls) - 1 if index == 0 else index - 1
    time.sleep(frequency)
