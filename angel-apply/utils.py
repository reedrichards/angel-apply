import logging
import os
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from settings import debug, email, password

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

SCRIPT_DIR = os.path.dirname(__file__)  # <-- absolute dir the script is in
REL_PATH = "angel-list-batch-apply.js"
ANGEL_SCRIPT_FILE_PATH = os.path.join(SCRIPT_DIR, REL_PATH)


def angel_browser() -> webdriver:
    """
    builds a logged in webdriver for angelist, connecting to a remote
    webdriver instance depending on the environment
    """
    # headless boilerplate
    chrome_options = Options()
    # chrome_options.add_argument("--disable-extensions")
    # chrome_options.add_argument("--disable-gpu")
    # headless lets us run without a window
    # chrome_options.add_argument("--headless")
    # make the user agent the same as what i have on desktop
    chrome_options.add_argument(
        (f"user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36"
         # space is here on purpose
         " (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36")
    )
    if debug:
        logging.info('connecting to local webdriver')
        browser = webdriver.Chrome(chrome_options=chrome_options)
    else:
        logging.info('connecting to remote webdriver')
        browser = webdriver.Remote("http://remote-webdriver:4444/wd/hub",
                                   DesiredCapabilities.CHROME)
    browser.get("https://angel.co/login")
    login_angelist(browser)
    return browser


def login_angelist(browser: webdriver) -> bool:
    """
    execute commands to login to angelist given a webdriver instance, email,
    and password
    """
    email_field = browser.find_element_by_id("user_email")
    password_field = browser.find_element_by_id("user_password")
    submit = browser.find_element_by_name("commit")
    email_field.send_keys(email)
    password_field.send_keys(password)
    submit.click()


def apply_and_exit(url: str):
    """
    applys to a job to first job result on angelist given a search url
    """
    browser = angel_browser()
    # wait for page to load
    time.sleep(15)
    browser.get(url)
    # wait for page to load
    time.sleep(7)
    result = browser.execute_script(open(ANGEL_SCRIPT_FILE_PATH).read())
    logging.info(result)
    # wait for script to run
    time.sleep(10)
    browser.quit()
