// This script targets all companies in your search results with a custom note.

// 1. Search Angel List for your location and keywords.
// 2. Paste this script into the console and run it.
// 3. Wait while it works.
// 4. Copy the results from the console for your records.

// You will probably need to run it several times, because it appears to get
// interrupted by Angel List's interspersed 'call to action' buttons in the search
// results. Fortunately, once you apply to a job, it hides from the results.

// this script has been modified from the original to only apply to the first job listing
// to avoid rate limiting from angel.co

// it is being executed from a headless chrome instance being run from a python script, every 15 minutes.
// for more information see README.org

function angelAppy() {

  var placeholderString,
  placeholderWords,
  contactName,
  companyName,
  customNote,
  jobDescription,
  jobUrl,
  jobInfo,
  str = '';

  var listings = $('.job_listings')

    .filter(function(_, elem) {
      return ($(elem).attr('data-force-note') );
    })
    // shift should only give us the first element, so that we avoid rate limiting
    // .shift()
    function apply(elem) {

      // Find the URL for each job listing.
      $(elem)
          .find('.top a[href]')
          .each( function(idx, value) { str += $(value).attr('href') + "\n"; });

      // Get the placeholder note so we can pluck the names of the contact and company.
      placeholderString = $(elem)
        .find('.interested-note').attr('placeholder');

      // Split placeholder string into words:
      placeholderWords = placeholderString.split(' ');

      // Pluck contact name
      contactName = placeholderWords[4];

      // Pluck company name
      companyName = $(elem).find('.startup-link').text();

      // Build personalized note
        customNote = "Hello! My name is Robert Wendt and I am a Django developer / Kubernetes guru. I have a tendency to automate everything I touch, and I enjoy working on go, Docker, Kubernetes, Python, and more! Do I sound like a good fit for your orginization? Let's chat! You can find my gitlab profile here https://gitlab.com/reedrichards and my personal website is https://grimeywebsites.com/. Looking forward to hearing from you soon!";

      // .header-info .tagline (text)
      jobDescription = $(elem).find('.tagline').text();

      // .header-info .startup-link (href attr)
      jobUrl = $(elem).find('.startup-link').attr('href');

      // Compile and format job information
      jobInfo = {
        "companyName":companyName,
         "jobDescription": jobDescription,
         "url": str
      }

      // Get job data for your own records
      console.log(jobInfo);

      // Log out custom note to verify syntax.
      // console.log(customNote + '\n');

      // Add your custom note.
      // *** (Comment these lines out to debug.)
      $(elem)
        .find('.interested-note').text( customNote );

      // Fire in the hole!
      // *** (Comment these lines out to debug.)
      $(elem)
        .find('.interested-with-note-button')
        .each( function(idx, button) { $(button).click(); });

      return jobInfo
    };

    // Print all of the company and job info to the console.
    // return jobInfo;
    return apply(listings[0])
}
return angelAppy()
